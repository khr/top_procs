#!/usr/bin/env python

"""
"""

import sys
import datetime
from collections import OrderedDict


def read_top_snapshot(filename):
    """Read output from top into an OrderedDict, with PIDs as keys.
    """
    top_dict = OrderedDict()
    main_part = False
    with open(filename, 'r') as fp:
        for _raw in fp:
            line = _raw.strip()
            if (not main_part) and line.startswith("PID"):
                main_part = True
                column_labels = line.split()
                continue
            if main_part:
                line_list = line.split()
                pid = line_list[0]
                line_dict = dict(zip(column_labels, line_list))
                minutes, seconds = line_dict["TIME+"].split(':')
                line_dict["TIME+"] = 60 * float(minutes) + float(seconds)
                top_dict[pid] = line_dict
    return top_dict


# read two top snapshots
top_1 = read_top_snapshot(sys.argv[1])
top_2 = read_top_snapshot(sys.argv[2])
common_pids = set(top_1.keys()).intersection(set(top_2.keys()))


# compute time differences for long-lived processes
dt_dict = OrderedDict()
for pid in common_pids:
    dt_dict[pid] = OrderedDict(top_2[pid])
    dt_dict[pid]["TIME+"] = top_2[pid]["TIME+"] - top_1[pid]["TIME+"]


# sort by time
dt_dict = OrderedDict(sorted(dt_dict.items(), key=lambda x: x[1]["TIME+"], reverse=True))


# table output of the top 20
print("{0:<16} {1}".format("CPU TIME [s]", "COMMAND"))
for i, pid in enumerate(dt_dict):
    print("{0:<16} {1}".format(dt_dict[pid]["TIME+"], dt_dict[pid]["COMMAND"]))
    if i > 19:
        break

