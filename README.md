Tool to determine the most expensive long-running processes
(services) on a Linux system, simply by comparing two outputs
of the `top` utility.

See the folder `slurm_example` for an example on how to use
the tool to inspect compute nodes integrated with a SLURM
batch system.

2019, Klaus Reuter, khr@mpcdf.mpg.de

