#!/bin/bash -l
#SBATCH -D ./
#SBATCH -J WRAPTOP
#SBATCH --partition=broadwell
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=40
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=none
#SBATCH --time=0:29:59

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

srun \
   wraptop_slurm \
      ./doflops.avx2 2 10 20

